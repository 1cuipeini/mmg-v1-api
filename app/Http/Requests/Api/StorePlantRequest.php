<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\PlantRequest;

class StorePlantRequest extends PlantRequest
{
    use ApiRequestTrait;
}
